import pytest

from solution import largest_area


def test_largest_area():
    biggest, size_within = largest_area("test_input", 32)
    assert biggest == 17
    assert size_within == 16
