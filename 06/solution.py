#!/usr/bin/env python

import datetime
from collections import defaultdict
from operator import itemgetter

import pandas as pd

OVERSHOOT_FACTOR = 1.25
FARTHEST_AWAY = 1000


def get_nearest_point(x, y, points, safe_total):
    nearest_distance = FARTHEST_AWAY
    nearest_point = None
    nearest_index = -999
    more_than_one = False
    total = 0
    for index, point in points.iterrows():
        distance = abs(x - point["x"]) + abs(y - point["y"])
        total += distance
        if distance == nearest_distance:
            more_than_one = True
        if distance < nearest_distance:
            more_than_one = False
            nearest_distance = distance
            nearest_point = point
            nearest_index = index
    if total < safe_total:
        within_safe_distance = True
    else:
        within_safe_distance = False
    if more_than_one:
        return None, None, within_safe_distance
    else:
        return nearest_index, nearest_point, within_safe_distance


def largest_area(points_file, safe_total):
    points = pd.read_csv(points_file, names=["x", "y"])
    x_edge = int(points["x"].max() * OVERSHOOT_FACTOR)
    y_edge = int(points["y"].max() * OVERSHOOT_FACTOR)
    area_map = dict()
    area_within_safe_distance = 0
    for x in range(x_edge):
        for y in range(y_edge):
            if x % 10 == 0 and y == 0:
                print(f"{datetime.datetime.now().isoformat()}: analyzing {x}, {y}.")
            nearest_index, nearest_point, within_safe_distance = get_nearest_point(
                x, y, points, safe_total
            )
            if within_safe_distance:
                area_within_safe_distance += 1
            if nearest_point is None:
                continue
            else:
                area_map[(x, y)] = (nearest_point["x"], nearest_point["y"])
                # print(
                #     f"The nearest to {x}, {y} is {nearest_point['x']}, {nearest_point['y']}"
                # )
    infinite_areas = set()
    for these_coordinates, nearest_point in area_map.items():
        if (
            these_coordinates[0] == (x_edge - 1)
            or these_coordinates[1] == (y_edge - 1)
            or these_coordinates[0] == 0
            or these_coordinates[1] == 0
        ):
            infinite_areas.add(nearest_point)
    areas_for_points = defaultdict(int)
    for these_coordinates, nearest_point in area_map.items():
        if nearest_point in infinite_areas:
            continue
        else:
            areas_for_points[nearest_point] += 1
    point, area = max(areas_for_points.items(), key=itemgetter(1))
    return area, area_within_safe_distance


def main():
    print(largest_area("input", 10_000))


if __name__ == "__main__":
    main()
