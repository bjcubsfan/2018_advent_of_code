#!/usr/bin/env python

from collections import defaultdict


def get_claim_inches(claim):
    """Return the list of coordinates for this claim.

    #1388 @ 362,926: 16x14         |
    \       \   \    \    \        |
     \       x   y   width height  |
     claim number                  |

    They go from the top left like:

    y x ->
    |
    v
    """
    number_part, rest = claim.split("@")
    claim_id = int(number_part[1:])
    coordinates, dimensions = rest.split(":")
    x, y = coordinates.split(",")
    width, height = dimensions.split("x")
    x, y, width, height = int(x), int(y), int(width), int(height)
    covered_inches = list()
    current_y = y + 1
    while height > 0:
        counting_width = width
        current_x = x + 1
        while counting_width > 0:
            covered_inches.append((current_x, current_y))
            counting_width -= 1
            current_x += 1
        current_y += 1
        height -= 1
    return claim_id, covered_inches


def overlap(input_data):
    claimed_inches = defaultdict(int)
    inches_per_claim = defaultdict(list)
    all_claims = list()
    for claim in input_data:
        if not claim:
            continue
        claim_id, this_claim_inches = get_claim_inches(claim)
        print(f"Loading claim #{claim_id}")
        all_claims.append(claim_id)
        inches_per_claim[claim_id].extend(this_claim_inches)
        for coordinates in this_claim_inches:
            claimed_inches[coordinates] += 1
    num_overlapping = 0
    print(f"Will review {len(claimed_inches)} coordinates.")
    for index, (coordinates, appearances) in enumerate(claimed_inches.items()):
        if (index + 1) % 1000 == 0:
            print(f"Reviewing coordinate {index + 1}/{len(claimed_inches)}.")
        if appearances > 1:
            num_overlapping += 1
            to_be_removed = []
            for claim_id in all_claims:
                if coordinates in inches_per_claim[claim_id]:
                    to_be_removed.append(claim_id)
                    break
            for claim_id in to_be_removed:
                all_claims.remove(claim_id)
    return num_overlapping, all_claims[0]


def main():
    with open("input") as input_data:
        input_data_one_line = input_data.read()
        print(overlap(input_data_one_line.split("\n")))


if __name__ == "__main__":
    main()
