import pytest

from solution import overlap


def test_overlap():
    example_input = """#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2"""
    inches_overlap, claim_id = overlap(example_input.split("\n"))
    assert inches_overlap == 4
    assert claim_id == 3
