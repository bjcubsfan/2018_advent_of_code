#!/usr/bin/env python:

from collections import defaultdict

INDEX_PRECURSOR = 5
INDEX_AFTERWARD = 36


def get_next_step(steps):
    new_steps = dict()
    available = list()
    for step, precursors in steps.items():
        if precursors:
            new_steps[step] = precursors
        else:
            # No precursors
            available.append(step)
    if len(available) > 1:
        available = sorted(available)
        next_step, rest = available[0], available[1:]
        for step in rest:
            # Add blank precursor
            new_steps[step] = []
    else:
        # Only one
        assert len(available) == 1
        next_step = available[0]
    for step, precursors in new_steps.items():
        if next_step in precursors:
            new_steps[step].remove(next_step)
    return next_step, new_steps


def order(input_steps):
    steps = defaultdict(list)
    for input_step in input_steps:
        precursor = input_step[INDEX_PRECURSOR]
        step_name = input_step[INDEX_AFTERWARD]
        steps[step_name].append(precursor)
        if precursor not in steps:
            steps[precursor]
    output = list()
    while steps:
        print(steps)
        next_step, steps = get_next_step(steps)
        output.append(next_step)
        print("".join(output))
        print(80 * "*")
    return "".join(output)


def main():
    with open("input") as input_data:
        steps = input_data.read().strip().split("\n")
        print(order(steps))


if __name__ == "__main__":
    main()
