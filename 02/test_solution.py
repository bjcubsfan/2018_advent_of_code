import pytest

from solution import checksum, commonality


def test_checksum():
    example_input = """abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab"""
    calculated_checksum = checksum(example_input.split("\n"))
    assert calculated_checksum == 12


def test_commonality():
    example_input = """"abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz"""
    common_letters = commonality(example_input.split("\n"))
    assert common_letters == "fgij"
