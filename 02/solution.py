#!/usr/bin/env python
from collections import defaultdict


def checksum(input_data):
    num_double_letters = 0
    num_triple_letters = 0
    for box_number in input_data:
        triple = False
        double = False
        letter_counts = defaultdict(int)
        for letter in box_number:
            letter_counts[letter] += 1
        print(letter_counts)
        for letter, count in letter_counts.items():
            if count == 3:
                triple = True
            elif count == 2:
                double = True
            else:
                continue
        if triple:
            num_triple_letters += 1
        if double:
            num_double_letters += 1
    return num_triple_letters * num_double_letters


def commonality(input_data):
    for box_number_1 in input_data:
        for box_number_2 in input_data:
            print(f">{box_number_1}< and >{box_number_2}<")
            num_differences = 0
            common_letters = []
            print('new box---------')
            for char_1, char_2 in zip(box_number_1, box_number_2):
                if char_1 == char_2:
                    common_letters.append(char_1)
                    continue
                else:
                    # Not the same
                    num_differences += 1
                if num_differences > 1:
                    break
            if num_differences == 1:
                return ''.join(common_letters)


def main():
    with open("input") as input_data:
        input_data_one = input_data.read()
        print(checksum(input_data_one.strip().split("\n")))
        print(commonality(input_data_one.strip().split("\n")))


if __name__ == "__main__":
    main()
