#!/usr/bin/env python

import string


def react_polymer(input_polymer):
    index = 0
    polymer = list(input_polymer)
    # print(len(polymer))
    while index < (len(polymer) - 1):
        # print(''.join(polymer))
        char_1 = polymer[index]
        char_2 = polymer[index + 1]
        if char_1 == char_2:
            index = index + 1
        else:
            # Not the same
            if char_1.lower() == char_2.lower():
                # Upper and lower pair, destroy
                # print(' ' * index + '^')
                del polymer[index : index + 2]
                if index != 0:
                    index = index - 1
            else:
                # Not a pair continue
                index = index + 1
    return polymer


def smallest_removing_one(input_polymer):
    shortest = 1e10
    polymer = list(input_polymer)
    for letter in string.ascii_lowercase:
        removed = []
        for polymer_part in polymer:
            if polymer_part.lower() != letter:
                removed.append(polymer_part)
        this_length = len(react_polymer(removed))
        if this_length < shortest:
            shortest = this_length
            print(f"found a short one by "
                  f"removing '{letter}' gets {this_length}")
    return shortest


def main():
    with open("input") as input_data:
        input_polymer = input_data.read().strip()
        print(len(react_polymer(input_polymer)))
        print(smallest_removing_one(input_polymer))


if __name__ == "__main__":
    main()
