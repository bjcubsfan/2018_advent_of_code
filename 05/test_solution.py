import pytest

from solution import react_polymer, smallest_removing_one


def test_react_polymer():
    input_polymer = "dabAcCaCBAcCcaDA"
    reacted = react_polymer(input_polymer)
    assert len(reacted) == 10


def test_smallest_removing_one():
    input_polymer = "dabAcCaCBAcCcaDA"
    smallest_length = smallest_removing_one(input_polymer)
    assert smallest_length == 4
