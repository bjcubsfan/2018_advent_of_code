#!/usr/bin/env python

from collections import defaultdict
from operator import itemgetter
import datetime


def parse_line(line):
    date_part, command_part = line.split("]")
    when = datetime.datetime.strptime(date_part, "[%Y-%m-%d %H:%M")
    command = command_part.strip()
    command = command.split()
    parsed_line = {"when": when, "command": command}
    return parsed_line


def fill_minutes(
    guard_minutes_asleep, guard_minute, guard, sleeping, previous_minute, current_minute
):
    for minute in range(previous_minute, current_minute):
        guard_minutes_asleep[guard] += 1
        guard_minute[guard][minute] += 1


def most_sleepy(input_data):
    lines = []
    for line in input_data:
        lines.append(parse_line(line))
    sorted_lines = sorted(lines, key=itemgetter("when"))
    guard_minutes_asleep = defaultdict(int)
    guard_minute = defaultdict(lambda: defaultdict(int))
    current_guard = -1
    sleeping = False
    previous_minute = -1
    for line in sorted_lines:
        current_minute = line["when"].minute
        if line["command"][0] == "Guard":
            # Change guard
            guard = int(line["command"][1][1:])
            if not previous_minute == -1:
                fill_minutes(
                    guard_minutes_asleep,
                    guard_minute,
                    guard,
                    sleeping,
                    previous_minute,
                    current_minute,
                )
            sleeping = False
        elif line["command"][0] == "wakes":
            fill_minutes(
                guard_minutes_asleep,
                guard_minute,
                guard,
                sleeping,
                previous_minute,
                current_minute,
            )
            sleeping = True
        elif line["command"][0] == "wakes":
            fill_minutes(
                guard_minutes_asleep,
                guard_minute,
                guard,
                sleeping,
                previous_minute,
                current_minute,
            )
            sleeping = False
        previous_minute = current_minute
    guard_most_asleep = max(guard_minutes_asleep.items(), key=itemgetter(1))[0]
    sleepys_minutes = guard_minute[guard_most_asleep]
    sleepiest_minute = max(sleepys_minutes.items(), key=itemgetter(1))[0]
    part_1 = sleepiest_minute * guard_most_asleep
    max_guard_minute = (0, 0, 0)
    for guard, minutes in guard_minute.items():
        minute, num_asleep = max(minutes.items(), key=itemgetter(1))
        if num_asleep > max_guard_minute[2]:
            max_guard_minute = (guard, minute, num_asleep)
    part_2 = max_guard_minute[0] * max_guard_minute[1]
    return part_1, part_2


def main():
    with open("input") as input_data:
        input_data_list = input_data.read().strip().split("\n")
        print(most_sleepy(input_data_list))


if __name__ == "__main__":
    main()
