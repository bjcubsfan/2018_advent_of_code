import pytest

from solution import calibrate, find_double

example_1 = """+1
-2
+3
+1"""

example_2 = """+1
+1
+1"""

example_3 = """+1
+1
-2"""

example_4 = """-1
-2
-3"""


@pytest.mark.parametrize(
    "changes, final", [(example_1, 3), (example_2, 3), (example_3, 0), (example_4, -6)]
)
def test_calibrate(changes, final):
    calculated = calibrate(changes.split("\n"))
    assert calculated == final


example_5 = """+1
-1"""

example_6 = """+3
+3
+4
-2
-4"""

example_7 = """-6
+3
+8
+5
-6"""

example_8 = """+7
+7
-2
-7
-4"""


@pytest.mark.parametrize(
    "changes, final",
    [(example_1, 2), (example_5, 0), (example_6, 10), (example_7, 5), (example_8, 14)],
)
def test_find_double(changes, final):
    double = find_double(changes.split("\n"))
    assert double == final
