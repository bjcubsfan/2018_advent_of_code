#!/usr/bin/env python


def cycle_through(input_data, total, seen):
    double_found = False
    for line in input_data:
        sign, number = line[0], line[1:]
        if sign == "+":
            total += int(number)
        else:
            # negative
            total -= int(number)
        if total in seen:
            double_found = True
            return total, seen, double_found
        else:
            seen.add(total)
    return total, seen, double_found


def find_double(input_data):
    total = 0
    seen = set()
    seen.add(total)
    while True:
        total, seen, double_found = cycle_through(input_data, total, seen)
        if double_found:
            return total


def calibrate(input_data):
    total = 0
    for line in input_data:
        sign, number = line[0], line[1:]
        if sign == "+":
            total += int(number)
        else:
            # negative
            total -= int(number)
    return total


def main():
    with open("input") as input_data:
        input_data_one = input_data.read()
        print(calibrate(input_data_one.strip().split("\n")))
        print(find_double(input_data_one.strip().split("\n")))


if __name__ == "__main__":
    main()
